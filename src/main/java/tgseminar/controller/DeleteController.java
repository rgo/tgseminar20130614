package tgseminar.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;

public class DeleteController extends Controller {

	@Override
	protected Navigation run() throws Exception {

		// Retrieve items from request parameter
		String id = super.request.getParameter("id");
		if (id == null || id.isEmpty()) {
			// if NULL or NOT SET, return HTTP 400
			super.response.setStatus(400);
			return null;
		}

		if (!id.matches("-?\\d+(\\.\\d+)?")) {
			// if not NUMERIC
			super.response.setStatus(400);
			return null;
		}

		Key key = Datastore.createKey("ToDo", Long.parseLong(id));
		Datastore.delete(key);
		super.response.setStatus(200);

		return null;
	}

}
