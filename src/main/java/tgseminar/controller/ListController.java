package tgseminar.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;
import org.slim3.memcache.Memcache;
import org.slim3.repackaged.org.json.JSONException;
import org.slim3.repackaged.org.json.JSONObject;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;

public class ListController extends Controller {

	@Override
	protected Navigation run() throws Exception {
		List<Entity> results = null;

		User user = UserServiceFactory.getUserService().getCurrentUser();

		if (Memcache.contains(user.getEmail())) {
			results = Memcache.get(user.getEmail());
			if (results != null) {
				System.out.println("Yeah! Memcache!");
				super.response.setContentType("application/json");
				super.response.setCharacterEncoding("utf-8");
				super.response.getWriter().print(this.convertToJson(results));
				super.response.flushBuffer();
				super.response.setStatus(200);
				return null;
			}
		}

		List<Map<String, Object>> retObj = new ArrayList<Map<String,Object>>();
		Map<String, Object> innerItem;
		JSONObject retJson = new JSONObject();

		results =
			Datastore.query("ToDo")
				.filter("createdBy", FilterOperator.EQUAL, user.getEmail())
				.sort("createdAt", SortDirection.ASCENDING)
				.asList();

		for (Entity item : results) {
			innerItem = new HashMap<String, Object>();
			innerItem.put("id", item.getKey().getId());
			innerItem.put("title", item.getProperty("title"));
			innerItem.put("createdBy", item.getProperty("createdBy"));
			innerItem.put("createdAt", item.getProperty("createdAt"));
			retObj.add(innerItem);
		}
		retJson.put("", retObj);

		super.response.setContentType("application/json");
		super.response.setCharacterEncoding("utf-8");
		super.response.getWriter().print(this.convertToJson(results));
		super.response.flushBuffer();
		super.response.setStatus(200);
		return null;
	}

	private String convertToJson(List<Entity> results) throws JSONException {
		List<Map<String, Object>> retObj = new ArrayList<Map<String,Object>>();
		Map<String, Object> innerItem;
		for (Entity item : results) {
			innerItem = new HashMap<String, Object>();
			innerItem.put("id", item.getKey().getId());
			innerItem.put("title", item.getProperty("title"));
			innerItem.put("createdBy", item.getProperty("createdBy"));
			innerItem.put("createdAt", item.getProperty("createdAt"));
			retObj.add(innerItem);
		}
		return JSONObject.valueToString(retObj);
	}
}
