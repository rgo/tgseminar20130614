package tgseminar.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.users.UserServiceFactory;

public class UpdateController extends Controller {

	@Override
	protected Navigation run() throws Exception {
		int id = 0;

		String idStr = super.request.getParameter("id");
		String title = super.request.getParameter("title");

		// [Validation] Required parameter: ID
		if (idStr == null || idStr.isEmpty()) {
			response.setStatus(400);
			return null;
		}

		// [Validation] ID parameter format: numeric
		try {
			id = Integer.parseInt(idStr);
		} catch (NumberFormatException nEx) {
			response.setStatus(400);
			return null;
		}

		// [Validation] Required parameter: Title
		if (title == null || title.isEmpty()) {
			response.setStatus(400);
			return null;
		}

		// Check if ID exists in any entities
		Key key = Datastore.createKey("ToDo", id);
		Entity entity = Datastore.getOrNull(key);
		if (entity == null) {
			response.setStatus(404);
			return null;
		}

		String email = UserServiceFactory.getUserService().getCurrentUser().getEmail();
		if (!email.equals(entity.getProperty("createdBy"))) {
			response.setStatus(403);
			return null;
		}

		entity.setProperty("title", title);
		Datastore.put(entity);
		response.setStatus(200);

		return null;
	}

}
