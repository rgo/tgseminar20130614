/**
 * Created by User-Hp003 on 6/13/13.
 */
///<reference path='../libs/DefinitelyTyped/angularjs/angular.d.ts' />
///<reference path='../Model.ts' />
///<reference path='../Service.ts' />

module Todo {
    export interface Scope extends ng.IScope {
        todos:Model.Todo[];

        add:()=>void;
        remove:()=>void;
        modify:(index:number)=>void;

        remove2:(index:number)=>void;
        modify2:(index:number,modifiedContent:string)=>void;

        newContent:string;
        removeIndex:number;
    }

    export class Controller {
        constructor(public $scope:Scope, public $window:ng.IWindowService, public todoService:Service.TodoService) {
            this.$scope.todos = [
                new Model.Todo("Hello, My ToDo")
            ];

            this.$scope.add = () => this.add();
            this.$scope.remove = () => this.remove();
            this.$scope.modify = (index:number) => this.modify(index);

            this.$scope.remove2 = (index) => this.remove2(index);
            this.$scope.modify2 = (index,modifiedContent) => this.modify2(index,modifiedContent);

            this.todoService.getList()
                .success((todos:Model.Todo[]) => {
                    this.$scope.todos = todos;
                })
        }

        /* Add values to current stack */
        add():void {
            this.$scope.todos.push(new Model.Todo({title:this.$scope.newContent}));
            this.$scope.newContent = "";
        }
        /* Remove value from current stack. Usage is to set globally defined index to delete. */
        remove():void {
            this.$scope.todos.splice(this.$scope.removeIndex, 1);
            this.$scope.removeIndex = undefined;
        }
        modify(index:number):void {
            var newValue = this.$window.prompt("新値を入力してください", this.$scope.todos[index].title);
            this.$scope.todos[index].title = newValue;
        }

        /* Remove value from current stack using parameter index. */
        remove2(index):void {
            this.$scope.todos.splice(index, 1);
        }
        /* Modify value based on parameter index and new value. */
        modify2(index,modifiedContent):void {
            this.$scope.todos[index] = new Model.Todo({title:modifiedContent});
        }
    }
}
