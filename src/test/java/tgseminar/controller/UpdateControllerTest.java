package tgseminar.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;

import org.hamcrest.core.IsNot;
import org.junit.Test;
import org.slim3.datastore.Datastore;
import org.slim3.tester.ControllerTestCase;
import org.slim3.tester.TestEnvironment;

import com.google.appengine.api.datastore.Entity;
import com.google.apphosting.api.ApiProxy;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class UpdateControllerTest extends ControllerTestCase {

	@Override
	public void setUp() throws Exception {
		super.setUp();

		Entity entity = new Entity(Datastore.createKey("ToDo", 1));
		entity.setProperty("title", "Todo #1");
		entity.setProperty("createdBy", "test@example.com");
		entity.setProperty("createdAt", new Date());

		Entity entity1 = new Entity(Datastore.createKey("ToDo", 2));
		entity1.setProperty("title", "Todo #2");
		entity1.setProperty("createdBy", "test2@example.com");
		entity1.setProperty("createdAt", new Date());

		Datastore.put(entity, entity1);

		TestEnvironment env = (TestEnvironment) ApiProxy
				.getCurrentEnvironment();
		env.setEmail("test@example.com");
	}

	@Test
	public void respond400IfIdNotSpecified() throws NullPointerException,
			IllegalArgumentException, IOException, ServletException {

		// tester.param("id", "1");
		tester.param("title", "ToDo #1");
		tester.start("/Update");

		// assert response from server is 400
		assertThat(tester.response.getStatus(), is(400));
	}

	@Test
	public void respond400IfIdNotNumber() throws NullPointerException,
			IllegalArgumentException, IOException, ServletException {
		tester.param("id", "aaaa");
		tester.start("/Update");
		assertThat(tester.response.getStatus(), is(400));
	}

	@Test
	public void respond400IfTitleNotSpecified() throws NullPointerException,
			IllegalArgumentException, IOException, ServletException {
		tester.param("id", "12345");
		tester.start("/Update");
		assertThat(tester.response.getStatus(), is(400));
	}

	@Test
	public void respond404IfEntityNotExists() throws NullPointerException,
			IllegalArgumentException, IOException, ServletException {
		tester.param("id", "3");
		tester.param("title", "ToDo #3 Modified");
		tester.start("/Update");
		assertThat(tester.response.getStatus(), is(404));
	}

	@Test
	public void respond403IfEntityInAnotherUser() throws NullPointerException,
			IllegalArgumentException, IOException, ServletException {

		Entity entity = Datastore.getOrNull(Datastore.createKey("ToDo", 2));
		assertThat(entity, is(notNullValue()));
		assertThat((String) entity.getProperty("createdBy"),
				is("test2@example.com"));

		tester.param("id", "2");
		tester.param("title", "Todo #3 Modified");
		tester.start("/Update");
		assertThat(tester.response.getStatus(), is(403));
	}

	@Test
	public void respond200IfSuccess() throws NullPointerException,
			IllegalArgumentException, IOException, ServletException {

		Entity entity = Datastore.getOrNull(Datastore.createKey("ToDo", 1));
		assertThat(entity, is(notNullValue()));
		assertThat((String) entity.getProperty("createdBy"),
				is("test@example.com"));

		tester.param("id", "1");
		tester.param("title", "Todo #1 Modified");
		tester.start("/Update");
		assertThat(tester.response.getStatus(), is(200));

		entity = Datastore.getOrNull(Datastore.createKey("ToDo", 1));
		assertThat((String) entity.getProperty("title"), is("Todo #1 Modified"));
	}
}
